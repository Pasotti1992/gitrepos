package matteopasotti.com.gitrepos.api;


import java.util.List;

import matteopasotti.com.gitrepos.data.Stargazer;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitHubApiInterface {

    @GET("/repos/{owner}/{repo}/stargazers")
    Call<List<Stargazer>> getStargazers(@Path("owner") String owner, @Path("repo") String repo);
}
