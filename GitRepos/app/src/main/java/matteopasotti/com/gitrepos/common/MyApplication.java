package matteopasotti.com.gitrepos.common;

import android.app.Application;

import matteopasotti.com.gitrepos.dagger.component.DaggerNetComponent;
import matteopasotti.com.gitrepos.dagger.component.NetComponent;
import matteopasotti.com.gitrepos.dagger.module.AppModule;
import matteopasotti.com.gitrepos.dagger.module.NetModule;


public class MyApplication extends Application {

    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(Constants.BASE_URL))
                .build();
    }

    public NetComponent getNetComponent() { return netComponent; }
}
