package matteopasotti.com.gitrepos.dagger.component;


import dagger.Component;
import matteopasotti.com.gitrepos.dagger.module.MainModule;
import matteopasotti.com.gitrepos.mainscreen.MainActivity;
import matteopasotti.com.gitrepos.util.ActivityScope;

@Component(dependencies = NetComponent.class, modules = MainModule.class)
@ActivityScope
public interface MainComponent {

    void inject(MainActivity mainActivity);
}
