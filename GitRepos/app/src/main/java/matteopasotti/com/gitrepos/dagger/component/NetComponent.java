package matteopasotti.com.gitrepos.dagger.component;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import matteopasotti.com.gitrepos.dagger.module.AppModule;
import matteopasotti.com.gitrepos.dagger.module.NetModule;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {

    Retrofit retrofit();
    Application provideApplication();
}
