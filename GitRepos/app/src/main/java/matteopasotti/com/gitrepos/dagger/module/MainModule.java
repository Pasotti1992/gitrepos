package matteopasotti.com.gitrepos.dagger.module;


import android.support.v7.widget.LinearLayoutManager;
import android.widget.LinearLayout;

import dagger.Module;
import dagger.Provides;
import matteopasotti.com.gitrepos.mainscreen.MainActivity;
import matteopasotti.com.gitrepos.mainscreen.MainContract;
import matteopasotti.com.gitrepos.mainscreen.StargazerAdapter;
import matteopasotti.com.gitrepos.util.ActivityScope;

@Module
public class MainModule {

    private final MainContract.View mView;

    private MainActivity mainActivity;

    public MainModule(MainContract.View mView, MainActivity mainActivity) {
        this.mView = mView;
        this.mainActivity = mainActivity;
    }

    @Provides
    @ActivityScope
    MainContract.View provideMainView() {return mView; }

    @Provides
    @ActivityScope
    StargazerAdapter provideStargazerAdapter() {
        return new StargazerAdapter();
    }

    @Provides
    @ActivityScope
    LinearLayoutManager provideLinearLayoutManager() {
         return new LinearLayoutManager(mainActivity, LinearLayoutManager.VERTICAL, false);
    }
}
