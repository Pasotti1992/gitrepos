package matteopasotti.com.gitrepos.mainscreen;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import matteopasotti.com.gitrepos.R;
import matteopasotti.com.gitrepos.common.MyApplication;
import matteopasotti.com.gitrepos.dagger.component.DaggerMainComponent;
import matteopasotti.com.gitrepos.dagger.module.MainModule;
import matteopasotti.com.gitrepos.data.Stargazer;


public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.ownerEditText)
    EditText ownerEdit;

    @BindView(R.id.repoEditText)
    EditText repoEdit;

    @BindView(R.id.buttonSearch)
    Button buttonSearch;

    @BindView(R.id.buttonReset)
    Button buttonReset;

    @BindView(R.id.list)
    RecyclerView list;

    @Inject
    MainPresenter mPresenter;

    @Inject
    StargazerAdapter adapter;

    @Inject
    LinearLayoutManager layoutManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

        DaggerMainComponent.builder()
                .netComponent(((MyApplication) getApplicationContext()).getNetComponent())
                .mainModule(new MainModule(this, this))
                .build().inject(this);

        initView();

    }

    private void initView() {
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);
    }

    @OnClick(R.id.buttonSearch)
    public void startSearch() {
        if(!ownerEdit.getText().toString().equals("") && !repoEdit.getText().toString().equals("")) {
            mPresenter.getStargazers(ownerEdit.getText().toString(), repoEdit.getText().toString());
        } else {
            showToast("Complete the fields!");
        }
    }

    @OnClick(R.id.buttonReset)
    public void cleanFields(){
        repoEdit.setText("");
        ownerEdit.setText("");
    }

    @Override
    public void initStargazersList(List<Stargazer> items) {
        adapter.initStargazers(items);
    }

    @Override
    public void updateStargazersList(List<Stargazer> stargazers) {

    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean loading) {

    }
}
