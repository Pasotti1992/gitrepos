package matteopasotti.com.gitrepos.mainscreen;


import java.util.List;

import matteopasotti.com.gitrepos.data.Stargazer;

public interface MainContract {

    interface View {

        void initStargazersList(List<Stargazer> items);

        void updateStargazersList(List<Stargazer> stargazers);

        void showToast(String message);

        void showLoading(boolean loading);
    }

    interface Presenter {

        void getStargazers(String owner, String repo);
    }
}
