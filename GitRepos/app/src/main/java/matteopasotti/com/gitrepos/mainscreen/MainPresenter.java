package matteopasotti.com.gitrepos.mainscreen;


import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import matteopasotti.com.gitrepos.api.GitHubApiInterface;
import matteopasotti.com.gitrepos.data.Stargazer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainPresenter implements MainContract.Presenter {

    Retrofit retrofit;

    MainContract.View mView;

    @Inject
    public MainPresenter(Retrofit retrofit, MainContract.View view) {
        this.retrofit = retrofit;
        this.mView = view;
    }

    @Override
    public void getStargazers(String owner, String repo) {

        retrofit.create(GitHubApiInterface.class).getStargazers(owner, repo).enqueue(new Callback<List<Stargazer>>() {
            @Override
            public void onResponse(Call<List<Stargazer>> call, Response<List<Stargazer>> response) {
                if(response.body() != null && response.body().size() > 0) {
                    mView.initStargazersList(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Stargazer>> call, Throwable t) {
                Log.d("MainPresenter", "getStargazers FAILED" + t.toString());
                mView.showToast("Error getStargazers FAILED");
            }
        });
    }
}
