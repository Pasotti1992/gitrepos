package matteopasotti.com.gitrepos.mainscreen;


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import matteopasotti.com.gitrepos.R;
import matteopasotti.com.gitrepos.data.Stargazer;

public class StargazerAdapter extends RecyclerView.Adapter<StargazerAdapter.StargazerHolder> {

    private List<Stargazer> items;

    @Inject
    public StargazerAdapter(){
        items = new ArrayList<>();
    }


    @Override
    public StargazerHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.user_row, parent, false);

        StargazerHolder holder = new StargazerHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(StargazerHolder holder, int position) {

        Stargazer item = items.get(position);

        holder.name.setText(item.getLogin());

        Uri uri = Uri.parse(item.getAvatar_url());
        Glide.with(holder.itemView.getContext()).load(uri)
                .thumbnail(0.5f)
                .crossFade()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void initStargazers(List<Stargazer> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public static class StargazerHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_image)
        ImageView image;

        @BindView(R.id.user_name)
        TextView name;

        public StargazerHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
